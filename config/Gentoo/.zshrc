PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="arrow"
ENABLE_CORRECTION="true"
plugins=(git zsh-autosuggestions zsh-syntax-highlighting)
source $ZSH/oh-my-zsh.sh
source /home/ali/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
export PATH=/home/ali/.local/bin:/home/ali/.local/lib/python3.9/site-packages/*:$PATH
export FZF_DEFAULT_COMMAND='fdfind --type f'
export FZF_DEFAULT_OPTS='--layout=reverse --inline-info --height=80%'
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
autoload -U compinit promptinit
compinit
promptinit; prompt gentoo
zstyle ':completion::complete:*' use-cache 1
#Correct Persian,Arabic Text Direction in Terminal

if ! [[ "$(ps -p $(ps -p $(echo $$) -o ppid=) -o comm=)" =~ 'bicon'* ]]; then

 bicon.bin

 fi
clear
#cat /home/ali/Config/banner | lolcat
echo "\n"
neofetch
