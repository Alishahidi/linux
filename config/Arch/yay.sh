
# Install yay
cd ~
cd Project
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
# end

# install aur packages

yay -S --noconfirm i3lock-color polybar siji-git xss-lock mugshot papirus-icon-theme materia-gtk-theme flat-remix-gtk

# end

